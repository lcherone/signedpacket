<?php
namespace SignedPacket;

use Encryption\Encryption;
use Base91\Base91;

class SignedPacket {

    public function __construct($publicKey = null, $privateKey = null)
    {
        $this->encryption = true;
        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * Rehashes the public and private keys to create the final key used for
     * encryption/decryption
     *
     * @return string (32)
     */
    private function encryptionKey()
    {
        return hash(
            'ripemd128',
            str_rot13($this->publicKey.$this->privateKey)
        );
    }

    /**
     * Encrypts, wraps and signs the data into a payload packet
     *
     * @param array $packet
     * @return array
     */
    public function encodePacket($packet = array())
    {

        $data = json_encode($packet);

        if ($this->encryption) {
            $crypt = new Encryption();

            $data = $crypt->encrypt($data, $crypt->makeKey(
                $this->encryptionKey(),
                $this->publicKey,
                $this->privateKey
            ));
            $data = Base91::encode($data);
        }

        $packet = array(
            'data'         => $data,
            'public_key'   => $this->publicKey,
            'request_time' => time()
        );

        $packet['token'] = hash_hmac('sha256', $packet['data'], $this->privateKey);

        if ($this->encryption) {
            $packet = Base91::encode(json_encode($packet));
        }

        return $packet;
    }

    /**
     * Checks valid packet then decrypts and returns the original data
     *
     * @param array $packet
     * @return object
     */
    public function decodePacket($packet = array())
    {
        if ($this->encryption) {
            $packet = json_decode(Base91::decode($packet), true);
        }

        // packet validation
        if (!$this->validatePacket($packet)) {
            return false;
        }

        if ($this->encryption) {
            $crypt = new Encryption();

            $packet['data'] = $crypt->decrypt(Base91::decode($packet['data']), $crypt->makeKey(
                $this->encryptionKey(),
                $this->publicKey,
                $this->privateKey
            ));
        }

        return json_decode($packet['data']);
    }

    /**
     * Validate the the client request packet.
     *
     *  Packets received are first encrypted on the client, the encrypted data
     *  is then signed with a mutual private key, which creates that stateless
     *  security layer where as no passwords are needed, only the public key.
     *  Without the private key packet validation and decryption is impossible.
     *
     * Note: If the entire packet is base91 encoded then the only error will be
     *       unauthorised or corrupt packet.
     *
     * @param array $packet
     * @return bool
     */
    public function validatePacket($packet = array())
    {
        $this->packet_state = 'valid';

        if (empty($packet['public_key'])) {
            $this->packet_state = 'missing public key';
            return false;
        }

        if (empty($packet['token'])) {
            $this->packet_state = 'missing token key';
            return false;
        }

        if (empty($packet['data'])) {
            $this->packet_state = 'empty data';
            return false;
        }

        if ($packet['public_key'] !== $this->publicKey) {
            $this->packet_state = 'unauthorised public key';
            return false;
        }

        // validate packet signature
        if (hash_hmac('sha256', $packet['data'], $this->privateKey) == $packet['token']) {
            return true;
        } else {
            $this->packet_state = 'unauthorised';
            return false;
        }
    }
}
