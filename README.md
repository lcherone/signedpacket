# Secure Signed Stateless Packet #

##Why do it?##
To implement and secure the 2 way communications between, say point A and point B, without sending passwords/keys or have a pre-authentication mechanism in place.

This is where signed packets comes in, simply put A signed packet is when you digitally sign the data with mutually known public
and private keys. Then on the receiver the data is then resigned using the known keys and if the tokens match then its verified that the packet has come from a valid host.

This mitigates MITM attacks to the point an attacker can only resend the same packet and forging becomes impossible as the attacker does not have the private key.

##Implemention##
I’ve implemented hashmac CBC based encryption so the whole packet is encrypted and then base91 encoded, which on its own is I suppose is enough security but the idea is to make it tighter then a emo guy's pants!!!

##So...##
This package makes it easy for you to sign (and optionally encrypt) a packet of data using public and private keys, simplez!..

###Install with Composer###
```
#!json
{
    "minimum-stability": "dev",
    "require": {
        "lcherone/signed-packet": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:lcherone/signedpacket.git"
        }
    ]
}
```

###Usage###
```
#!php
<?php
require 'vendor/autoload.php';

use SignedPacket\SignedPacket;

$packet = new SignedPacket('aPubl1cKey', 'aPrivateKey');

echo $encoded = $packet->encodePacket(array('hello world'));

echo '<pre>'.print_r($packet->decodePacket($encoded), true).'</pre>';
```
